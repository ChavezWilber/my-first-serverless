function ValidarNaves(data) {
  let {
    Nombre,
    Modelo,
    Fabricante,
    Costo,
    Longitud,
    VelocidadMaxAtmosferica,
    Tripulacion,
    Pasajeros,
    CapacidadCarga,
    TiempoMaxConsumo,
    TipoImpulsor,
    Velocidad,
    Clase,
    Piloto,
    Pelicula,
  } = data;

  let response = {
    status: true,
    message: "ok",
  };
  let i = 0;
  Nombre === undefined ? (response = { status: false, message: "Nombre" }) : (i = 0);
  Modelo === undefined ? (response = { status: false, message: "Modelo" }) : (i = 0);
  Fabricante === undefined ? (response = { status: false, message: "Fabricante" }) : (i = 0);
  Costo === undefined ? (response = { status: false, message: "Costo" }) : (i = 0);
  Longitud === undefined ? (response = { status: false, message: "Longitud" }) : (i = 0);
  VelocidadMaxAtmosferica === undefined ? (response = { status: false, message: "VelocidadMaxAtmosferica" }) : (i = 0);
  Tripulacion === undefined ? (response = { status: false, message: "Tripulacion" }) : (i = 0);
  Pasajeros === undefined ? (response = { status: false, message: "Pasajeros" }) : (i = 0);
  CapacidadCarga === undefined ? (response = { status: false, message: "CapacidadCarga" }) : (i = 0);
  TiempoMaxConsumo === undefined ? (response = { status: false, message: "TiempoMaxConsumo" }) : (i = 0);
  TipoImpulsor === undefined ? (response = { status: false, message: "TipoImpulsor" }) : (i = 0);
  Velocidad === undefined ? (response = { status: false, message: "Velocidad" }) : (i = 0);
  Clase === undefined ? (response = { status: false, message: "Clase" }) : (i = 0);
  Piloto === undefined ? (response = { status: false, message: "Piloto" }) : (i = 0);
  Pelicula === undefined ? (response = { status: false, message: "Pelicula" }) : (i = 0);

  return response;
}
function ValidarPersonas(data) {
  let { nombre, Tamaño, masa, ColorCabello, TipoCabello, ColorOjos, FechNacimiento, Sexo, PlanetaOrigen, Peliculas, Especies, Vehiculos, Naves } =
    data;

  let response = {
    status: true,
    message: "ok",
  };
  let i = 0;
  nombre === undefined ? (response = { status: false, message: "nombre" }) : (i = 0);
  Tamaño === undefined ? (response = { status: false, message: "Tamaño" }) : (i = 0);
  masa === undefined ? (response = { status: false, message: "masa" }) : (i = 0);
  ColorCabello === undefined ? (response = { status: false, message: "ColorCabello" }) : (i = 0);
  TipoCabello === undefined ? (response = { status: false, message: "TipoCabello" }) : (i = 0);
  ColorOjos === undefined ? (response = { status: false, message: "ColorOjos" }) : (i = 0);
  FechNacimiento === undefined ? (response = { status: false, message: "FechNacimiento" }) : (i = 0);
  Sexo === undefined ? (response = { status: false, message: "Sexo" }) : (i = 0);
  PlanetaOrigen === undefined ? (response = { status: false, message: "PlanetaOrigen" }) : (i = 0);
  Peliculas === undefined ? (response = { status: false, message: "Peliculas" }) : (i = 0);
  Especies === undefined ? (response = { status: false, message: "Especies" }) : (i = 0);
  Vehiculos === undefined ? (response = { status: false, message: "Vehiculos" }) : (i = 0);
  Naves === undefined ? (response = { status: false, message: "Naves" }) : (i = 0);

  return response;
}

function ValidarEspecies(data) {
  let {
    nombre,
    clasificacion,
    designacion,
    AlturaPromedio,
    ColorPiel,
    ColorCabello,
    ColorOjos,
    PromedioVida,
    PlanetaOringen,
    Lenguaje,
    Personas,
    Peliculas,
  } = data;

  let response = {
    status: true,
    message: "ok",
  };
  let i = 0;
  nombre === undefined ? (response = { status: false, message: "nombre" }) : (i = 0);
  clasificacion === undefined ? (response = { status: false, message: "clasificacion" }) : (i = 0);
  designacion === undefined ? (response = { status: false, message: "designacion" }) : (i = 0);
  AlturaPromedio === undefined ? (response = { status: false, message: "AlturaPromedio" }) : (i = 0);
  ColorPiel === undefined ? (response = { status: false, message: "ColorPiel" }) : (i = 0);
  ColorCabello === undefined ? (response = { status: false, message: "ColorCabello" }) : (i = 0);
  ColorOjos === undefined ? (response = { status: false, message: "ColorOjos" }) : (i = 0);
  PromedioVida === undefined ? (response = { status: false, message: "PromedioVida" }) : (i = 0);
  PlanetaOringen === undefined ? (response = { status: false, message: "PlanetaOringen" }) : (i = 0);
  Lenguaje === undefined ? (response = { status: false, message: "Lenguaje" }) : (i = 0);
  Personas === undefined ? (response = { status: false, message: "Personas" }) : (i = 0);
  Peliculas === undefined ? (response = { status: false, message: "Peliculas" }) : (i = 0);

  return response;
}

module.exports = { ValidarNaves, ValidarPersonas, ValidarEspecies };
