function TransPersonas(data) {
  let {
    name,
    height,
    mass,
    hair_color,
    skin_color,
    eye_color,
    birth_year,
    gender,
    homeworld,
    films,
    species,
    vehicles,
    starships,
    created,
    edited,
    url,
  } = data;

  let DataTr = {
    Nombre: name,
    Altura: height,
    Masa: mass,
    ColorCabello: hair_color,
    ColorPiel: skin_color,
    ColorOjos: eye_color,
    AñoNacimiento: birth_year,
    Sexo: gender,
    PlanetaOrigen: homeworld,
    Peliculas: films,
    Especies: species,
    Vehiculos: vehicles,
    Naves: starships,
    FechCreacion: created,
    FechActualizacion: edited,
    url: url,
  };

  return DataTr;
}

function TransPeliculas(data) {
  let {
    title,
    episode_id,
    opening_crawl,
    director,
    producer,
    release_date,
    characters,
    planets,
    starships,
    vehicles,
    species,
    created,
    edited,
    url,
  } = data;

  let DataTr = {
    titulo: title,
    NroEpisodio: episode_id,
    ParrafoIinicial: opening_crawl,
    Director: director,
    Productor: producer,
    FechEstreno: release_date,
    PersonasPelicula: characters,
    Planetas: planets,
    Naves: starships,
    Vehiculos: vehicles,
    Especies: species,
    FechCreacion: created,
    FechaActualizacion: edited,
    url: url,
  };

  return DataTr;
}

function TransNaves(data) {
  let {
    name,
    model,
    manufacturer,
    cost_in_credits,
    length,
    max_atmosphering_speed,
    crew,
    passengers,
    cargo_capacity,
    consumables,
    hyperdrive_rating,
    MGLT,
    starship_class,
    pilots,
    films,
    created,
    edited,
    url,
  } = data;

  let DataTr = {
    Nombre: name,
    Modelo: model,
    Fabricante: manufacturer,
    Costo: cost_in_credits,
    Longitud: length,
    VelocidadMaxAtmosferica: max_atmosphering_speed,
    Tripulacion: crew,
    Pasajeros: passengers,
    CapacidadCarga: cargo_capacity,
    TiempoMaxConsumo: consumables,
    TipoImpulsor: hyperdrive_rating,
    Velocidad: MGLT,
    Clase: starship_class,
    Piloto: pilots,
    Pelicula: films,
    FechCreacion: created,
    FechaActualizacion: edited,
    url: url,
  };

  return DataTr;
}

module.exports = { TransPersonas, TransPeliculas, TransNaves };
