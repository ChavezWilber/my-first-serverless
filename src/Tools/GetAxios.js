const axios = require("axios");

async function axiosFuncions(url) {
  let response = await axios.get(url);
  return response.data;
}

module.exports = axiosFuncions;
