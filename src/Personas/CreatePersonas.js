/**
 *
 * @author AJ Catambay | Bridging Code 2020
 *
 */
"use strict";

const AWS = require("aws-sdk");
const uuid = require("uuid");
const { ValidarPersonas } = require("../Tools/Validar");

const dynamoDb = new AWS.DynamoDB.DocumentClient();

module.exports.crearPersonas = (event, context, callback) => {
  const datetime = new Date().toISOString();
  const data = JSON.parse(event.body);

  let validaror = {
    status: true,
    message: "ok",
  };

  validaror = ValidarPersonas(data);

  if (!validaror.status) {
    console.error("Campos Imcompletos");
    const response = {
      statusCode: 400,
      body: JSON.stringify({ message: "Peticion incompleta: " + validaror.message }),
    };
    callback(null, response);
    return;
  }

  const params = {
    TableName: "people",
    Item: {
      id: uuid.v1(),
      nombre: data.nombre,
      Tamaño: data.Tamaño,
      masa: data.masa,
      ColorCabello: data.ColorCabello,
      TipoCabello: data.TipoCabello,
      ColorOjos: data.ColorOjos,
      FechNacimiento: data.FechNacimiento,
      Sexo: data.Sexo,
      PlanetaOrigen: data.PlanetaOrigen,
      Peliculas: data.Peliculas,
      Especies: data.species,
      Vehiculos: data.vehicles,
      Naves: data.starships,
      url: "http://" + event.requestContext.domainName + "/" + event.requestContext.path,
      done: false,
      FechCreacion: datetime,
      FechActualualizacion: null,
    },
  };

  dynamoDb.put(params, (error, data) => {
    if (error) {
      console.error(error);
      callback(new Error(error));
      return;
    }

    const response = {
      statusCode: 201,
      body: JSON.stringify(params.Item),
    };

    callback(null, response);
  });
};
