/**
 *
 * @author AJ Catambay | Bridging Code 2020
 *
 */
"use strict";

const axiosFuncions = require("../Tools/GetAxios");
const { TransNaves } = require("../Tools/Transformar");

const url = "https://swapi.py4e.com/api/starships/9/";

async function TransformarNav() {
  let data = await axiosFuncions(url);
  let trans = TransNaves(data);
  console.log(trans);
  return trans;
}

module.exports.TransfNaves = async () => {
  const data = await TransformarNav();
  console.log(data);
  const response = {
    statusCode: 200,
    body: JSON.stringify(data),
  };
  return response;
};
