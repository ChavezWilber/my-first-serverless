/**
 *
 * @author AJ Catambay | Bridging Code 2020
 *
 */
"use strict";

const axiosFuncions = require("../Tools/GetAxios");
const { TransPersonas } = require("../Tools/Transformar");

const url = "https://swapi.py4e.com/api/people/1/";

async function TransformarPer() {
  let data = await axiosFuncions(url);
  let trans = TransPersonas(data);
  console.log(trans);
  return trans;
}

module.exports.TransfPersonas = async () => {
  const data = await TransformarPer();
  console.log(data);
  const response = {
    statusCode: 200,
    body: JSON.stringify(data),
  };
  return response;
};
