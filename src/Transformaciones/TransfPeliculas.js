/**
 *
 * @author AJ Catambay | Bridging Code 2020
 *
 */
"use strict";

const axiosFuncions = require("../Tools/GetAxios");
const { TransPeliculas } = require("../Tools/Transformar");

const url = "https://swapi.py4e.com/api/films/1/";

async function TransformarPeli() {
  let data = await axiosFuncions(url);
  let trans = TransPeliculas(data);
  console.log(trans);
  return trans;
}

module.exports.TransfPeliculas = async () => {
  const data = await TransformarPeli();
  console.log(data);
  const response = {
    statusCode: 200,
    body: JSON.stringify(data),
  };
  return response;
};
