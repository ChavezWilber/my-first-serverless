/**
 *
 * @author AJ Catambay | Bridging Code 2020
 *
 */
"use strict";

const AWS = require("aws-sdk");
const uuid = require("uuid");
const { ValidarEspecies } = require("../Tools/Validar");

const dynamoDb = new AWS.DynamoDB.DocumentClient();

module.exports.crearEspecie = (event, context, callback) => {
  const datetime = new Date().toISOString();
  const data = JSON.parse(event.body);

  let validaror = {
    status: true,
    message: "ok",
  };

  validaror = ValidarEspecies(data);

  if (!validaror.status) {
    console.error("Campos Imcompletos");
    const response = {
      statusCode: 400,
      body: JSON.stringify({ message: "Peticion incompleta: " + validaror.message }),
    };
    callback(null, response);
    return;
  }

  const params = {
    TableName: "especies",
    Item: {
      id: uuid.v1(),
      nombre: data.nombre,
      clasificacion: data.clasificacion,
      designacion: data.designacion,
      AlturaPromedio: data.AlturaPromedio,
      ColorPiel: data.ColorPiel,
      ColorCabello: data.ColorCabello,
      ColorOjos: data.ColorOjos,
      PromedioVida: data.PromedioVida,
      PlanetaOringen: data.PlanetaOringen,
      Lenguaje: data.Lenguaje,
      Personas: data.Personas,
      Peliculas: data.Peliculas,
      url: "http://" + event.requestContext.domainName + "/" + event.requestContext.path,
      done: false,
      FechCreacion: datetime,
      FechActualualizacion: null,
    },
  };

  dynamoDb.put(params, (error, data) => {
    if (error) {
      console.error(error);
      callback(new Error(error));
      return;
    }

    const response = {
      statusCode: 201,
      body: JSON.stringify(params.Item),
    };

    callback(null, response);
  });
};
