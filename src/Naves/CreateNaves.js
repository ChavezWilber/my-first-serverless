/**
 *
 * @author AJ Catambay | Bridging Code 2020
 *
 */
"use strict";

const AWS = require("aws-sdk");
const uuid = require("uuid");
const { ValidarNaves } = require("../Tools/Validar");

const dynamoDb = new AWS.DynamoDB.DocumentClient();

module.exports.crearNaves = (event, context, callback) => {
  const datetime = new Date().toISOString();
  const data = JSON.parse(event.body);

  let validaror = {
    status: true,
    message: "ok",
  };

  validaror = ValidarNaves(data);

  if (!validaror.status) {
    console.error("Campos Imcompletos");
    const response = {
      statusCode: 400,
      body: JSON.stringify({ message: "Peticion incompleta: " + validaror.message }),
    };
    callback(null, response);
    return;
  }

  const params = {
    TableName: "naves",
    Item: {
      id: uuid.v1(),
      Nombre: data.Nombre,
      Modelo: data.Modelo,
      Fabricante: data.Fabricante,
      Costo: data.Costo,
      Longitud: data.Longitud,
      VelocidadMaxAtmosferica: data.VelocidadMaxAtmosferica,
      Tripulacion: data.Tripulacion,
      Pasajeros: data.Pasajeros,
      CapacidadCarga: data.CapacidadCarga,
      TiempoMaxConsumo: data.TiempoMaxConsumo,
      TipoImpulsor: data.TipoImpulsor,
      Velocidad: data.Velocidad,
      Clase: data.Clase,
      Piloto: data.Piloto,
      Pelicula: data.Pelicula,
      FechCreacion: data.created,
      FechaActualizacion: data.edited,
      url: data.url,
    },
  };

  dynamoDb.put(params, (error, data) => {
    if (error) {
      console.error(error);
      callback(new Error(error));
      return;
    }

    const response = {
      statusCode: 201,
      body: JSON.stringify(params.Item),
    };

    callback(null, response);
  });
};
