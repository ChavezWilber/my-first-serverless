<!--
title: 'AWS Simple HTTP Endpoint example in NodeJS'
description: 'This template demonstrates how to make a simple HTTP API with Node.js running on AWS Lambda and API Gateway using the Serverless Framework.'
layout: Doc
framework: v3
platform: AWS
language: nodeJS
authorLink: 'https://github.com/serverless'
authorName: 'Serverless, inc.'
authorAvatar: 'https://avatars1.githubusercontent.com/u/13742415?s=200&v=4'
-->

# The Star Wars API  utilizando Nodejs y AWS


Este proyecto tiene como finalidad realizar 3 tipos de actividades : CREAR, LISTAR Y TRANSFORMAR.

## Usage

### Instalar dependencias
```
$ npm i
```

### Deployment

```
$ serverless deploy
```

![ESPECIE](https://gitlab.com/ChavezWilber/my-first-serverless/-/wikis/uploads/d647d517fc87da4fd7dc2d66520d0fcf/DEPLOY.png)


## Crear
Para poder crear un item dependiendo del escenario se utilizarán los siguientes ENDPOINTS
 
#### Crear un item para las especies
https://810ywu5hng.execute-api.eu-west-3.amazonaws.com/dev/addespecies

Peticion ### Post
```json
 {

  "nombre": "Yadorak", 
    "clasificacion": "mammal", 
    "designacion": "sentient", 
    "AlturaPromedio": "210", 
    "ColorPiel": "gray", 
    "ColorCabello": "black, brown", 
    "ColorOjos": "blue, green, yellow, brown, golden, red", 
    "PromedioVida": "400", 
    "PlanetaOringen": "https://swapi.py4e.com/api/planets/14/", 
    "Lenguaje": "Shyriiwook", 
    "Personas": [
        "https://swapi.py4e.com/api/people/13/", 
        "https://swapi.py4e.com/api/people/80/"
    ], 
    "Peliculas": [
        "https://swapi.py4e.com/api/films/1/", 
        "https://swapi.py4e.com/api/films/2/", 
        "https://swapi.py4e.com/api/films/3/", 
        "https://swapi.py4e.com/api/films/6/", 
        "https://swapi.py4e.com/api/films/7/"
    ]
}
```

![ESPECIE](https://gitlab.com/ChavezWilber/my-first-serverless/-/wikis/uploads/32b4dbd702a95110407e0c92b944c1d9/especie.png)

#### Crear un item para las personas
https://810ywu5hng.execute-api.eu-west-3.amazonaws.com/dev/addpeople

```json
 {

 {
  "nombre": "Nombre de prueba 1ssss",
  "Tamaño": "172",
  "masa": "77",
  "ColorCabello": "blond",
  "TipoCabello": "fair",
  "ColorOjos": "blue",
  "FechNacimiento": "19BBY",
  "Sexo": "male",
  "PlanetaOrigen": "https://swapi.py4e.com/api/planets/1/",
  "Peliculas": [
    "https://swapi.py4e.com/api/films/1/",
    "https://swapi.py4e.com/api/films/2/",
    "https://swapi.py4e.com/api/films/3/",
    "https://swapi.py4e.com/api/films/6/",
    "https://swapi.py4e.com/api/films/7/"
  ],
  "Especies": ["https://swapi.py4e.com/api/species/1/"],
  "Vehiculos": ["https://swapi.py4e.com/api/vehicles/14/", "https://swapi.py4e.com/api/vehicles/30/"],
  "Naves": ["https://swapi.py4e.com/api/starships/12/", "https://swapi.py4e.com/api/starships/22/"]
}

}
```
![persona](https://gitlab.com/ChavezWilber/my-first-serverless/-/wikis/uploads/26c6f3dd8dad1a79510c6fa35e88f791/persona.png)

#### Crear un item para las naves
https://810ywu5hng.execute-api.eu-west-3.amazonaws.com/dev/addnaves
 
```json
 {

 {
  "Nombre": "Death Star",
  "Modelo": "DS-1 Orbital Battle Station",
  "Fabricante": "Imperial Department of Military Research, Sienar Fleet Systems",
  "Costo": "1000000000000",
  "Longitud": "120000",
  "VelocidadMaxAtmosferica": "n/a",
  "Tripulacion": "342,953",
  "Pasajeros": "843,342",
  "CapacidadCarga": "1000000000000",
  "TiempoMaxConsumo": "3 years",
  "TipoImpulsor": "4.0",
  "Velocidad": "10",
  "Clase": "Deep Space Mobile Battlestation",
  "Piloto": [],
  "Pelicula": ["https://swapi.py4e.com/api/films/1/"],
  "created": "2014-12-10T16:36:50.509000Z",
  "edited": "2014-12-20T21:26:24.783000Z",
  "url": "https://swapi.py4e.com/api/starships/9/"
}

}
```

![NAVE](https://gitlab.com/ChavezWilber/my-first-serverless/-/wikis/uploads/b93d79cf721db4779dc2ebfb14ad8f7d/naves.png)


## Listar
Para poder listar un item dependiendo del escenario se utilizarán los siguientes ENDPOINTS

### Listar Especie
✔ https://810ywu5hng.execute-api.eu-west-3.amazonaws.com/dev/getespecie
### Listar Persona
✔ https://810ywu5hng.execute-api.eu-west-3.amazonaws.com/dev/getpeople
### Listar nave
✔ https://810ywu5hng.execute-api.eu-west-3.amazonaws.com/dev/getnave

## TRANSFORMAR
Para poder transformar los items dependiendo del escenario tomado de ejemplo se utilizarán los siguientes ENDPOINTS

### Listar Pelicula
  #### ENDPOINT Original
  ✔ https://swapi.py4e.com/api/films/1/
  #### ENDPOINT Transformado
  ✔ https://810ywu5hng.execute-api.eu-west-3.amazonaws.com/dev/transpeli
 
### Listar Persona
  #### ENDPOINT Original
  ✔ https://swapi.py4e.com/api/people/1/
  #### ENDPOINT Transformado
  ✔ https://810ywu5hng.execute-api.eu-west-3.amazonaws.com/dev/transpers

### Listar Nave
  #### ENDPOINT Original
  ✔ https://swapi.py4e.com/api/starships/9/
  #### ENDPOINT Transformado
  ✔ https://810ywu5hng.execute-api.eu-west-3.amazonaws.com/dev/transnave


 
